# fraud-detection-challenge
This repository contains the sorce code and resulting models of aData Science challenge for fraud detection.

To check the code and the documentation please take a look at the [Python Notebook](https://nbviewer.jupyter.org/github/rooom13/fraud-detection-challenge/blob/master/fraud-detection.ipynb).

An overview of the challenge resolution can be found [here](https://docs.google.com/presentation/d/1d0SHrK0UfTlrKyqB5UtIZhEhDC1lLIGJkaLeFEzF6GE/edit?usp=sharing)
